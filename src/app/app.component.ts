import { Component } from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})

export class AppComponent {
    title = 'favorites';
    imageRoot = 'assets/images/';

    public bookmarkGroups: any = [
        { label: 'Reddit', url: 'https://www.reddit.com/', image: this.imageRoot + 'reddit.jpg', tags:[] },
        { label: '/r/unixporn', url: 'https://www.reddit.com/r/unixporn', image: this.imageRoot + 'unixporn.jpg', tags:[] },
        { label: '/r/startpages', url: 'https://www.reddit.com/r/startpages', image: this.imageRoot + 'startpages.jpg', tags:[] },
        { label: '/r/EarthPorn', url: 'https://www.reddit.com/r/EarthPorn', image: this.imageRoot + 'earthporn.jpg', tags:[] },
        { label: '/r/Patchuu', url: 'https://www.reddit.com/r/Patchuu', image: this.imageRoot + 'patchuu.jpg', tags:[] },
        { label: 'University', url: 'https://www.uni-due.de', image: this.imageRoot + 'university.jpg', tags:[] },
        { label: 'Faculty', url: 'https://www.wiwi.uni-due.de/studium/angewandte-informatik/bsc-ai-se/informationen', image: this.imageRoot + 'faculty.jpg', tags:[] },
        { label: 'Studentcouncil', url: 'https://fse.uni-due.de', image: this.imageRoot + 'studentcouncil.jpg', tags:[] },
        { label: 'Nextcloud', url: 'https://nc.mos4.de', image: this.imageRoot + 'nextcloud.jpg', tags:[] },
        { label: 'Plex', url: 'https://127.0.0.1:32400/web', image: this.imageRoot + 'plex.jpg', tags:[] },
        { label: 'hledger', url: 'https://127.0.0.1:5000/journal', image: this.imageRoot + 'hledger.jpg', tags:[] },
        { label: 'WhatsApp', url: 'https://web.whatsapp.com', image: this.imageRoot + 'whatsapp.jpg', tags:[] },
        { label: 'Telegram', url: 'https://web.telegram.org', image: this.imageRoot + 'telegram.jpg', tags:[] }
    ]
}
