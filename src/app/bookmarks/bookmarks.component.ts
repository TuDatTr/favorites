import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-bookmarks',
  templateUrl: './bookmarks.component.html',
  styleUrls: ['./bookmarks.component.scss']
})
export class BookmarksComponent implements OnInit {
  @Input()
  public bookmarks;

  constructor() { }

  ngOnInit() {
  }

  onBookmarkClick(link: any) {
    window.open(link, '_blank');
  }

  filterBookmarks(filter: string) {
      console.log(typeof(this.bookmarks));
  }
}
